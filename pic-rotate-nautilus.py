'''
 Image Fixer Nautilus Plugin

 Remove Exif Rotation Info / Shrink / Rotate / PDF images extension for the 
 Nautilus file manager

 @version    2.0.2
 @package    pic-rotate-nautilus
 @copyright  Copyright (c) 2020 Martin Sauter
 @license    GNU General Public License v3
 @since      Since Release 1.0.0

 This Nautilus plugin performs the following actions depending on which 
 checkboxes the user activates: 
 
 * Starts when right-clicking on a DIRECTORY in Nautilus and selecting
   'EXTRA - Rotate/Shrink/PDF Pics in Directory'
 
 * Removes exif rotation information from different places
  (while leaving all other exif information in place) of a jpg file. 
 
 * As most smartphone cameras take the picture in landscape mode the 
   plugin then rotates the picture by 90 degrees into portrait mode
   
 * Shrinks the image by 50%. The shrink operation is only performed if 
   the image file contains rotation information in its exif header. Such
   information is typically present when the image comes from a camera and is
   removed by this add-on. If exif rotate information is present it indicates
   that the image was not treated by this add-on before and hence
   it is considered save to shrink the image. This way it is prevented
   that the image is shrunk several times in a row and thus made unusable.
 
 * Creates a PDF file from all jpg files in the directory (not checked by 
   default)
   
 * Splits pages in the created PDF file in half. Good option when an image
   contains two book pages which should be shown on separate pages. Works
   on output of "Create PDF" so the directory will contain two PDF files
   after the process
 
 Tested on:
 
   Ubuntu 16.04
   Ubuntu 18.04
   Ubuntu 20.04
 
 Installation / Debugging, etc. 
 
 See README.md
 
 Python Version:
   
 * Even Ubuntu 20.04 still uses the Python 2.7 environment for 
   nautilus-python extensions. As pip for python2 is NOT installed in 
   Ubuntu 20.04 it is not possible to add any other Python packages. One
   can only work with those present by default in /usr/lib/python2.7
   
'''

# For Nautilus interaction
from gi.repository import Nautilus, GObject, Gio
import urllib
import os, subprocess
import glob
import threading
import multiprocessing
import time
import logging
from gi.repository import GLib, Gtk
from __builtin__ import True, False
import hashlib
import syslog
# Delete any imports after haslib that have been unintentionally added 
# by the Eclipse Python plugin.

# Uncomment the command below to log activity to a file
# Note: path must be absolute, it can't start at '~/...'  !!!
#logging.basicConfig(filename='log-pic-rotate.txt',level=logging.INFO, \
                    #format='%(asctime)s %(message)s')


class NautilusRotateShrinkPic(GObject.GObject, Nautilus.MenuProvider):
    def __init__(self):

        logging.info('Nautilus Rotate-Shrink Object initialized')        


    def menu_activate_cb(self, menu, file):

        logging.info('Menu entry selected')        

        if file.is_gone():
            return

        dir_name = urllib.unquote(file.get_uri())[7:]
        logging.info('Directory name : ' + str(dir_name))
        
        RotateShrink = RotateShrinkWithGtkInfoWindow(dir_name)
        RotateShrink.execute()        
        

    def get_file_items(self, window, files):

        logging.info('get_file_items()')

        # Only single selections allowed
        if len(files) != 1:
            logging.info('More than one entry selected, exiting')
            return

        file = files[0]       

        if file.get_file_type() != Gio.FileType.DIRECTORY:
            logging.info('File is a NOT A DIRECTORY, exiting')
            return

        # Get the directory name and remove the URI part (file://)
        dir_name = urllib.unquote(file.get_uri())[7:]
        logging.info("Directory name: " + str(dir_name))      

        item = Nautilus.MenuItem(name='Nautilus::rotate_shrink_pics',
                                 label='EXTRA - Rotate/Shrink/PDF Pics in Directory',
                                 tip='-')
        item.connect('activate', self.menu_activate_cb, file)
        return item,


class RotateShrinkWithGtkInfoWindow():
    """
    This class encapsulates all functionality to rotate/shrink all .jpg
    files in a directory and the GUI that interacts with the user
    """
    
    # UI components
    win = None
    grid = None
    bbox = None
    
    cb1 = None
    cb2 = None
    cb3 = None
    cb4 = None
    cb5 = None

    bt1 = None
    bt2 = None
    
    progress_bar = None  
        
    # Other class variables
    thread = None
    operation_canceled = False
    
    dir_name = None
   
    progress_bar_fill_state = 0
    
    p = None # Worker pool variable for multiprocessing operation    
    
    def __init__(self, rotate_path):

        logging.info('User Interface Initialization')

        # Copy the given path to an object variable for later use
        self.dir_name = rotate_path
        
        # Create the info window
        self.win = Gtk.Window(title='Image Cleanup')
        
        self.win.connect("destroy", self.__title_quit_bt_pressed)

        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(6)
        self.grid.set_column_spacing(6)
        self.grid.set_margin_top(10)
        self.grid.set_margin_bottom(10)
        self.grid.set_margin_left(15)
        self.grid.set_margin_right(15)
        self.grid.set_column_homogeneous(False)
        self.win.add(self.grid)

        self.cb1 = Gtk.CheckButton(label="Clean Exif")
        self.cb1.set_active(True)
        
        self.cb2 = Gtk.CheckButton(label="Rotate")
        self.cb2.set_active(True)
        
        self.cb3 = Gtk.CheckButton(label="Shrink")        
        self.cb3.set_active(False)
                
        self.cb4 = Gtk.CheckButton(label="PDF")        
        self.cb4.set_active(False)
        
        self.cb5 = Gtk.CheckButton(label="PDF Page Split")        
        self.cb5.set_active(False)

        
        self.grid.attach(self.cb1, 1, 1, 1, 1)
        self.grid.attach_next_to(self.cb2, self.cb1, Gtk.PositionType.RIGHT, 1, 1)
        self.grid.attach_next_to(self.cb3, self.cb2, Gtk.PositionType.RIGHT, 1, 1)
        self.grid.attach_next_to(self.cb4, self.cb3, Gtk.PositionType.RIGHT, 1, 1)
        self.grid.attach_next_to(self.cb5, self.cb4, Gtk.PositionType.RIGHT, 1, 1)

        # Attach a progress bar to the grid. 
        # Note: The progress bar has to span ALL columns. At the moment
        # there are 5 column, hence parameter four is set to 5
        self.progress_bar = Gtk.ProgressBar(show_text=True)
        self.grid.attach_next_to(self.progress_bar, self.cb1, Gtk.PositionType.BOTTOM, 5, 1)

        # Create a Button Box and attach it on the grid BELOW the progress bar
        # Note: The button box has to span ALL columns. At the moment
        # there are 5 column, hence parameter four is set to 5        
        self.bbox = Gtk.ButtonBox.new(Gtk.Orientation.HORIZONTAL)
        self.bbox.set_spacing(5)        
        self.bbox.set_layout(Gtk.ButtonBoxStyle.CENTER)
        self.grid.attach_next_to(self.bbox, self.progress_bar, Gtk.PositionType.BOTTOM, 5, 1)

        self.bt1 = Gtk.Button(label="Run")
        self.bt1.connect("clicked", self.__run_bt_clicked)
        self.bbox.add(self.bt1)    
        
        self.bt2 = Gtk.Button(label="Cancel")
        self.bt2.connect("clicked", self.__cancel_bt_clicked)
        #self.grid.attach(self.bt2, 4, 3, 1, 1)    
        self.bbox.add(self.bt2)
        
        self.grid.set_focus_chain((self.bbox, self.cb1, self.cb2, \
            self.cb3, self.cb4, self.cb5))        

    
    def execute(self):
        """
        This is the main method shows the window created in the class
        initialization function above and returns immediately. The user
        can then select the actions to be taken. When the run button is
        clicked, the __run_bt_clicked() is called to executed the desired
        function.
        """
       
        self.win.show_all()
        
        # Main UI loop. It is left when the window is closed in example_target()        
        Gtk.main()


    def __run_bt_clicked(self, button):
        """
        RUN button handler: Starts all actions the user has selected
        in the window, e.g. exif clean, shrink, rotate, PDF generation
        """

        # No further selection/modification of options allowed once the
        # run button was pressed
        self.bt1.set_sensitive(False)
        self.cb1.set_sensitive(False)
        self.cb2.set_sensitive(False)
        self.cb3.set_sensitive(False)
        self.cb4.set_sensitive(False)
        self.cb5.set_sensitive(False)

        self.thread = threading.Thread(target=self.__run_rotate_shrink)    
        
        self.thread.daemon = True
        self.thread.start()

        
    def __cancel_bt_clicked(self, button):
        """
        Cancel all operations by closing the window
        """
        # Signal to the worker thread to abort
        self.operation_canceled = True
        
        # If multiprocessor workers are running, terminate them.
        if self.p != None:
            logging.info("CANCEL BUTTON PRESSED - TERMINATE WORKERS")
            self.p.terminate()
        
        # Close the main window: Note: By itself it does not cancel the
        # worker thread.
        self.win.close()     

    def __title_quit_bt_pressed(self, bt):
        """
        This handler function catches the event that is generated when the
        user clicks on the 'Close' button in the title of the window. It
        ensures that all ongoing operations are canceled before the window
        is destroyed
        """
        
        logging.info("close button in title clicked!!!")
        self.operation_canceled = True
        
        # If multiprocessor workers are running, terminate them.
        if self.p != None:
            logging.info("CANCEL BUTTON PRESSED - TERMINATE WORKERS")
            self.p.terminate()
        
        Gtk.main_quit();        


    def __update_progress(self, i, filename):
        """ 
        Private method that updates the GUI. It is called from the worker
        thread asynchronously via GLib.idle_add() 
        """        
        
        self.progress_bar.set_fraction(i)
        self.progress_bar.set_text(filename)
        
        return False


    def __run_rotate_shrink(self):
        """
        This private method is the worker thread that removes exif information
        and rotates + shrinks all images of a directory. 
        """

        # Get all jpg files in the directory
        file_list = self.__get_file_list();
        
        progress_bar_increment = self.__calc_progress_bar_increment(file_list)

        # Now perform all processing steps

        # Run the shrink function first because it checks if
        # EXIF rotation information is present in the image file which
        # indicates that the image hasn't been treated so far by by this
        # code.
        self.__shrink_images(progress_bar_increment, file_list)
        
        self.__remove_exif_info(progress_bar_increment, file_list)
        self.__rotate_images(progress_bar_increment, file_list)
        
        # Remove thumbnails used by Nautilus because they could be in the 
        # wrong orientation and might not be refreshed immediately if they
        # already exist. 
        self.__remove_thumbnails(file_list)
        
        self.__pdf_images()
        
        self.__split_pdf_pages()
            
        # All is done, close the window and end the GTK main loop!
        self.win.close()
        

    def __get_file_list(self):
        os.chdir(self.dir_name)        
        file_list = glob.glob("*.jpg")
        file_list += glob.glob("*.JPG")
        file_list += glob.glob("*.jpeg")
        file_list += glob.glob("*.JPEG")

        # Now sort the file list alphabetically. This is required for
        # PDF generation to assemble the images in a PDF file in the correct 
        # order (i.e. ordered by filename)
        file_list.sort()        
        
        return file_list

        
    def __calc_progress_bar_increment(self, file_list):
        """ 
        Calculate the progress bar increment in percent for each treated
        file. The increment depends on how many options are selected
        """
        num_files = len(file_list)
        # prevent div by 0 error
        if num_files == 0: num_files = 1
        logging.info("Number of files: " + str(num_files))         
        
        num_opt_selected = 0
        if self.cb1.get_active(): num_opt_selected += 1
        if self.cb2.get_active(): num_opt_selected += 1
        if self.cb3.get_active(): num_opt_selected += 1
        # Note: cb4 and cb5 PDF options are fast one shot processes so they
        # have no impact on the progress bar
        
        # Prevent a div by 0 error
        if num_opt_selected == 0: num_opt_selected = 1

        progress_bar_increment = (100 / float(num_files)) / (100 * num_opt_selected)

        logging.info('bar increment: ' + str(progress_bar_increment))
        return progress_bar_increment


    def __runParallelTasks(self, progress_bar_increment, file_list, worker_function):
        """
        Uses Python's multiprocessing library to run several workers at the 
        same time. Note: This is NOT Python threading which can only perform
        serial multitasking but real multiprocessing with which work can be
        distributed and run simultaneously on several CPU cores
        """

        self.p = multiprocessing.Pool(multiprocessing.cpu_count())
        
        def callback_worker_finished(result_text):
            logging.info('A worker has finished: ' + result_text)        
           
            self.__trigger_progress_bar_increment(progress_bar_increment,
                                                  result_text)        
        
        for file in file_list:
            logging.info('Starting worker for: ' + file)
            self.p.apply_async(worker_function, args=(self.dir_name, file), 
                               callback=callback_worker_finished)

        # All work is in the pipeline, now wait for completion and then 
        # remove the worker pool resources, threads and processes again         
        self.p.close()
        self.p.join()
        self.p = None
       
        logging.info('ALL WORKERS FINSIHSED')         

    def __remove_exif_info(self, progress_bar_increment, file_list):

        if self.cb1.get_active():
            
            # Remove exif info in several parallel processes
            if self.operation_canceled == True: return
            syslog.syslog('Pic-Rotate EXIF remove: ' + self.dir_name)
            self.__runParallelTasks(progress_bar_increment, file_list, 
                                    mp_remove_exif_info)            
                
    def __rotate_images(self, progress_bar_increment, file_list):

        if self.cb2.get_active():
            
            # Remove exif info in several parallel processes
            if self.operation_canceled == True: return
            syslog.syslog('Pic-Rotate rotate: ' + self.dir_name)
            self.__runParallelTasks(progress_bar_increment, file_list, 
                                    mp_rotate_image_cmd)                         

                
    def __shrink_images(self, progress_bar_increment, file_list):
        
        if self.cb3.get_active():
            
            # Remove exif info in several parallel processes
            if self.operation_canceled == True: return
            syslog.syslog('Pic-Rotate shrink: ' + self.dir_name)
            self.__runParallelTasks(progress_bar_increment, file_list, 
                                    mp_shrink_image_cmd)            

                
    def __pdf_images(self):
        '''
        Create PDF via img2pdf shell command.
        
        Note 1: A Python package for this operation from the same author is
        available that would make things much simpler. However, we are 
        running in Python 2.7 here, even in Ubuntu 20.04! Unfortunately, 
        Ubuntu 20.04 at the same time prevents additional Python 2.7 modules 
        to be installed. So while it's ugly running a shell command for this, 
        it works and uses Python 3.
        
        Note 2: As we can't use file_list here, shell globing is used to
        assemble the list of image files that should go into the PDF.
        This requires globing to be set to case insensitive to include
        jpg, JPG, jpeg and JPEG! 
        
        Note 3: Python uses sh in os.system() which can't do case-insensitive
        globing. Therefore a wrapper is used to run the shell commands in 
        bash.
        '''
                
        if self.cb4.get_active():
            syslog.syslog('Pic-Rotate PDF: ' + self.dir_name)
            shell_command = "/bin/bash -c \"cd \\\"" + self.dir_name + "\\\"; " + \
                  "shopt -s nocaseglob; img2pdf *.jp* --output combined.pdf\""
            logging.info(shell_command)
            os.system(shell_command)            


    def __split_pdf_pages(self):
        if self.cb5.get_active():
            syslog.syslog('Pic-Rotate PDF split: ' + self.dir_name)
            shell_command = ("cd \"" + self.dir_name + 
                             "\"; mutool poster -x 2 combined.pdf combined-split.pdf")
            logging.info(shell_command)
            os.system(shell_command) 

                
    def __remove_thumbnails(self, file_list):        
        for file in file_list:
            if self.operation_canceled == True: return
            
            # Convert the filename into URL format, i.e. spaces and non-ASCII
            # characters are converted into escape codes.
            # Example: A space character becomes %20
            # French and other non-ASCII chars are also replaced
            #
            # It seems we are running Python < 3 here, so urrlib.parse.quote()
            # doesn't work. Instead I'm using the Python 2.x variant
            #
            # Note: +,/ and a few other chars are not encoded, they are 
            # specifically excluded! No idea if I caught them all...
            #
            # Python 3 variant: filename = urllib.parse.quote(file)
            # (not sure how + and / are excluded, look-up when needed            
            uri_filename = 'file://' + urllib.quote(self.dir_name + '/' + file, "+/()'=")
                        
            # Convert the unicode string into a set of bytes for the hash function
            # below.
            uri_filename=uri_filename.encode('utf-8')
            
            # Generate the md5 hash of the file uri
            file_hash = hashlib.md5(uri_filename).hexdigest()
        
            # The thumbnail file is stored in the following folders:
            # 
            # ~/.cache/thumbnails/normal
            # ~/.cache/thumbnails/large
            #
            # Each folder may contain png thumbnail for the current file 
            # and its name is the md5 hash calculated earlier
            tb_filename_normal = os.path.join(os.path.expanduser(
                        '~/.cache/thumbnails/normal'), file_hash) + '.png'
            tb_filename_large = os.path.join(os.path.expanduser(
                        '~/.cache/thumbnails/large'), file_hash) + '.png'
                             
            # If the thumbnail generation previously failed for an image, a 
            # dummy png is put into ..../fail/gnome-thumbnail-factory. 
            # Create a path + filename for this this as well and delete it as
            # well if it exists.            
            tb_filename_fail = os.path.join(os.path.expanduser(
                        '~/.cache/thumbnails/fail/gnome-thumbnail-factory'),
                        file_hash) + '.png'                                                                                
                                                                                    
            if os.path.exists(tb_filename_normal):
                logging.info("Delete Thumbnail: " + tb_filename_normal)
                os.remove(tb_filename_normal)
            else:
                logging.info("Not found       : " + tb_filename_normal)
                
            if os.path.exists(tb_filename_large):
                logging.info("Delete Thumbnail: " + tb_filename_large)
                os.remove(tb_filename_large)
            else:
                logging.info("Not found       : " + tb_filename_large)
                
            if os.path.exists(tb_filename_fail):
                logging.info("Delete Thumbnail: " + tb_filename_fail)
                os.remove(tb_filename_fail)
            else:
                logging.info("Not found       : " + tb_filename_fail)

        
    def __trigger_progress_bar_increment(self, increment, filename):
        """
        The progress bar is handled by GTK in another thread and hence
        can't be updated directly in the worker thread. Instead, this method 
        adds __update_progress() into the GTK idle queue and then sets this
        thread to idle for a millisecond. This wakes up the GTK thread
        which then updates the progress bar.
        """
        self.progress_bar_fill_state += increment
        GLib.idle_add(self.__update_progress, self.progress_bar_fill_state, filename)
        time.sleep(0.01)        


"""
Multiprocessing functions - These need to be outside of any class. 
Also, they can't be in their own class even if the methods are made static.

Note: Shell commands are started with 'nice -17' to lower their priority so
they are not slowing down the GUI and other programs if a lot of images are 
processed.
"""

def mp_remove_exif_info(dir_name, file):
    logging.info('Remove exif worker called for: ' + file + " path: " + dir_name)
    
    shell_command = "cd \"" + dir_name + "\"; nice -n 17 exiftool -Orientation= " + \
        "-thumbnailimage= -ifd1:all= -overwrite_original \"" + file + "\""
    logging.info(shell_command)
    os.system(shell_command)

    logging.info("Worker is done: " + file)

    # Return string to be shown in progress dialog box    
    return "Clean exif: " + file



def mp_rotate_image_cmd(dir_name, file):
    logging.info('Rotate worker called for: ' + file + " path: " + dir_name)
   
    logging.info("Rotating 90 deg.: " + file)                      
    shell_command = "cd \"" + dir_name + "\";  " + \
           "nice -n 17 convert \"" + file + "\" -rotate 90 \"" + file + "\"" 
    logging.info(shell_command)
    os.system(shell_command)

    logging.info("Worker is done: " + file)

    # Return string to be shown in progress dialog box    
    return "Rotate: " + file



def mp_shrink_image_cmd(dir_name, file):
    logging.info('Shrink img worker called for: ' + file + " path: " + dir_name)

    """
    Check if the image contains exif information. If not, it was already 
    shrunken. In that case abandon the shrink operation for this file   
    """

    shell_command_show_exif = "cd \"" + dir_name + "\"; nice -n 17 exiftool -Orientation " + \
        "-thumbnailimage -ifd1:all \"" + file + "\""        
    result = subprocess.check_output(shell_command_show_exif, shell=True)

    #logging.info(result)
    logging.info('Exif Rotate Info Length:' + str(len(result)))
    
    if len(result) < 1:
        return ""

    """
    Shrink the image
    """
    logging.info("Shrinking: " + file)
    shell_command = "cd \"" + dir_name + "\"; " + \
          "nice -n 17 convert \"" + file + "\" -resize 50% \"" + file + "\"" 
    logging.info(shell_command)
    os.system(shell_command)
        
    logging.info("Worker is done: " + file)

    # Return string to be shown in progress dialog box    
    return "Shrinking: " + file

