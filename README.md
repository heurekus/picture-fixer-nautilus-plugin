# Image Fixer Nautilus Plugin (Nemo supported as well)

 This Nautilus plugin performs the following actions depending on which checkboxes the user activates: (does not recurse into subdirectories): 
 
* Removes exif rotation information from different places (while leaving all other exif information in place). 
 
* As most smartphone cameras take the picture in landscape mode, the plugin then rotates the picture by 90 degrees into portrait mode
   
* Shrinks the image by 50%. The shrink operation is only performed if the image file contains rotation information in its exif header. Such information is typically present when the image comes from a camera and is removed by this add-on. If exif rotate information is present it indicates that the image was not treated by this add-on before and hence it is considered save to shrink the image. This way it is prevented that the image is shrunk several times in a row and thus made unusable.
 

* Creates a PDF file from all jpg/jpeg files in the directory (not checked by default). The filename extension search is case-insensitive.

* Splits pages in the created PDF file in half. Good option when an image contains two book pages in landscape orientation which should be shown on separate pages. Works on output of the "Create PDF" option so the directory will contain two PDF files after the process.

**Important**: Images are changed in place, so changes CAN NOT be reverted! So make a backup before if necessary!

## Screenshots

![](images/menu-entry.png)

![](images/image-cleanup-dialog-box.png)

## OS Versions tested
 
* Ubuntu 16.04
* Ubuntu 18.04 
* Ubuntu 20.04
* Ubuntu 22.04 (Nemo version only)

## Installation
 
* Install the following packages: 
 
```
sudo apt install python-nautilus
sudo apt install libimage-exiftool-perl
sudo apt install imagemagick
sudo apt install img2pdf
sudo apt install mupdf-tools
```
  
* Create the following path and copy 'pic-rotate-nautilus.py' into it: 

```
mkdir ~/.local/share/nautilus-python/
mkdir ~/.local/share/nautilus-python/extensions
cp pic-rotate-nautilus.py ~/.local/share/nautilus-python/extensions
```
 
* Restart Nautilus: 
 
```
sudo killall nautilus
```

Note: All processes need to be terminated, including the desktop instance

## Nemo - Required Changes

Nemo is a fork of Nautilus used in some Linux distributions. It can also be used as an alternative filemanger in Ubuntu. The following changes are required to make this plugin work in Nemo:

* Use apt to install all non-Nautilus extensions described above.

* Install nemo python support:

```
sudo apt install nemo-python
```

* Copy 'pic-rotate-nautilus.py' executable to the following directory:

```
/usr/share/nemo-python/extensions
```

* Perform the following changes in the file. SED is your friend:

```
### Quick copy/paste to shell:

sudo sed -i 's/Nautilus/Nemo/g' pic-rotate-nautilus.py
sudo sed -i 's/urllib.unquote/urllib.parse.unquote/g' pic-rotate-nautilus.py
sudo sed -i 's/urllib.quote/urllib.parse.quote/g' pic-rotate-nautilus.py
sudo sed -i 's/from __builtin__ import True, False/ /g' pic-rotate-nautilus.py


# Details

sed -i 's/Nautilus/Nemo/g' pic-rotate-nautilus.py

# Replacement terms:

# Everything 'Nautilus' needs to be called 'Nemo'
Nautils --> Nemo

# Nemo uses Python 3, in which urrlib has been changed:
urllib.unquote ---> urllib.parse.unquote
urllib.quote ---> urllib.parse.quote

# Delete the following line
from __builtin__ import True, False
```

 * Now end all instances of Nemo (check with 'ps' that all threads are gone, sometimes one thread is still present without a window). Restart Nemo. Done!



## Debugging
   
* Run 'nautilus --no-desktop' in a shell
   
* If nautilus starts but the shell returns to an input prompt instead of 
  showing debug info, exit nautilus and then kill all instances:
  'sudo killall nautilus'. Then, run 'nautilus --no-desktop' again before
  opening another window

## Logging

* Enable logging to a file in pic-rotate-nautilus.py

```
# Uncomment the command below to log activity to a file
# Note: path must be absolute, it can't start at '~/...'  !!!
# logging.basicConfig(filename='log-pic-rotate.txt',level=logging.INFO, \
#                     format='%(asctime)s %(message)s')
```

* Then end all Nautilus tasks

```
sudo killall nautilus
```

 * Run nautilus from this directory as follows:

```
nautilus --no-desktop
```

 * The log file will then be created in the directory from which nautilus was started.
 * Things work when the shell does NOT return to the command line while this Nautilus instance is running
 * In a second shell monitor the log file. First, wait until it is created then do a 'tail' on it:

```
tail -f log-pic-rotate.txt
```

## Python Version:
   
Even Ubuntu 20.04 still uses the Python 2.7 environment for nautilus-python extensions. As pip for python2 is NOT installed in Ubuntu 20.04 it is not possible to add any other Python packages. One can only work with those present by default in /usr/lib/python2.7
